#Not pythonic way
import os
from contextlib import contextmanager
from time import time
from typing import List, Collection

sum_list = [1,2,3,4]
result = 0
for i in sum_list:
    result += i

# Pythonic way
sum_list = [1,2,3,4]
result = sum(sum_list)

#Not pythonic way
some_list = []
for i in range(1, 101):
    if not i % 19:
        some_list.append(i)

# Pythonic way
# 1) filter
# 2) list comprehension


try:
    data = open('sadsad')
except FileNotFoundError:
    print('Could not 1 ')
    data = None

try:
    data = open('sadsad')
except FileNotFoundError as e:
    print('Could not 2')
    print(str(e))
    #exit()
print('After exit')


def get_file_data():
    try:
        f = open('asdsad')
        g = f.read()
        return g
    except FileNotFoundError as e:
        print('Could not 3')
        return None

data = get_file_data()
if data is None:
    print('No data to work with')


print(max([1,2,3]))
print(min([1,2,3]))


if len(some_list) > 0:
    print(min(some_list))

def get_list():
    some_list = []
    for i in range(1, 101):
        if not i % 19:
            some_list.append(i)
    if len(some_list) == 0:
        raise ValueError('Could not find numbers mod to 19')
    return some_list
smth = 0
if smth: #=> bool(0) is True
    print("I won't be printed")

if smth == 0: #=> bool(0) is not True
    print("I'll be printed")

if not smth: #=> bool(0) is not True
    print("I'll be printed")

if len(some_list) > 0:
    pass

if some_list: # => bool(some_list) is True
    pass

# for None check
if data is None:
    pass

s1 = 1234567
s2 = 1234567
print(s1 == s2)
print(s1 is s2)
print(str(s1) == str(s2))
print(str(s1) is str(s2))

s1 = 1234567
s2 = 1234566
s2 += 1
print(s1 == s2)
print(s1 is s2)
print(str(s1) == str(s2))
print(str(s1) is str(s2))

ex = ValueError('some message')
type(ex) is ValueError
isinstance(ex, ValueError)

try:
    g = open('sadsad', 'w')
    g.write('sadsad')
except Exception:
    print('Ooops')
    #exit()
finally:
    g.close()

with open('1', 'w') as f:
    f.write('sadsad')


class CustomOpen:
    def __init__(self, name, flags='w'):
        self.obj = open(name, flags)
    def __enter__(self):
        self.time1 = time()
        return self.obj
    def __exit__(self, exc_type, exc_val, exc_tb):
        print(time()-self.time1)
        self.obj.close()

with CustomOpen('1') as f:
    f.write('asdasd')
print('after')


class CustomOpen:
    def __init__(self, name, flags='w'):
        self.obj = open(name, flags)
    def __enter__(self):
        self.t = open('asd', 'w+')
        self.time1 = time()
        return self.obj
    def __exit__(self, exc_type, exc_val, exc_tb):
        print(time()-self.time1)
        os.unlink(self.t.name)
        self.obj.close()

with CustomOpen('1') as f:
    f.write('asdasd')
print('Defaut list iter')
for item in [1,2,3,4]:
    print(item)
print('Call next manually')
some_list_for_iter = iter([1,2,3,4])
print(next(some_list_for_iter))

class Iterator:
    def __init__(self, data):
        self._data = data
        self._index = 0
    def __next__(self):
        elem = self._data[self._index]
        self._index += 1
        return elem

class Lists:
    def __init__(self, *args):
        self.data = args
    def __iter__(self):
        return Iterator(self.data)


class Lists2:
    def __init__(self, *args):
        self.data = args
        self.index = 0
    def __iter__(self):
        self.index = 0
        return self
    def __next__(self):
        if len(self.data) - 1 < self.index:
            raise StopIteration
        data = self.data[self.index]
        self.index += 1
        return data
print('Мій ітератор, горжусь ним')
items = Lists2(1,2,3,4)
for i in items:
    print(i)
print('after +')
for i in items:
    print(i)
print('after ++')

class Lists3:
    def __init__(self, *args):
        self.data: Collection = args
        self.index = 0
    def __iter__(self):
        return iter(self.data)

def generator_example(max_items):
    item = 0
    while item < max_items:
        outer_data = yield item
        print(outer_data)
        item += 1
    return 'item'

gen = generator_example(5)
print(next(gen))
print(gen.send(10))
print(next(gen))
print(next(gen))
print(next(gen))
#print(next(gen))
#print(next(gen))

for i in generator_example(5):
    print(i)

def range_g(start=0, end=10):
    s = start
    while s<end:
        yield s
        s += 1

@contextmanager
def o(name):
    f = open(name, 'a')
    yield f
    f.close()

with o('1') as f:
    f.write('sadsdasdasdasad')